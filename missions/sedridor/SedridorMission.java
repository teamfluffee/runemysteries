package scripts.modules.runemysteries.missions.sedridor;

import org.tribot.api2007.Game;
import org.tribot.api2007.Player;
import scripts.fluffeesapi.client.clientextensions.Interfaces;
import scripts.fluffeesapi.game.genericTasks.CloseQuestCompleted;
import scripts.fluffeesapi.game.genericTasks.TalkToGuide;
import scripts.fluffeesapi.data.structures.bag.BagIds;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.taskmissions.TaskMission;
import scripts.fluffeesapi.scripting.frameworks.task.TaskSet;
import scripts.modules.runemysteries.RuneMysteries;

public class SedridorMission implements TaskMission {

    @Override
    public TaskSet getTaskSet() {
        return new TaskSet(new TalkToGuide("Sedridor", "Talking to Sedridor",
                new String[]{"I'm looking for the head wizard.", "Ok, here you are.", "Yes, certainly."}),
                new CloseQuestCompleted());
    }

    @Override
    public String getMissionName() {
        return "Sedridor Chat";
    }

    @Override
    public String getBagId() {
        return BagIds.RUNE_MYSTERIES.getId();
    }

    @Override
    public boolean isMissionValid() {
        return (Game.getSetting(RuneMysteries.QUEST_SETTING) == 1 || Game.getSetting(RuneMysteries.QUEST_SETTING) == 2 ||
                Game.getSetting(RuneMysteries.QUEST_SETTING) == 5) || (Game.getSetting(RuneMysteries.QUEST_SETTING) == 6
                && Interfaces.isInterfaceValid(277)) && RuneMysteries.SEDRIDOR_AREA.contains(Player.getPosition());
    }

    @Override
    public boolean isMissionCompleted() {
        return Game.getSetting(RuneMysteries.QUEST_SETTING) == 3  || (Game.getSetting(RuneMysteries.QUEST_SETTING) >= 6
                && !Interfaces.isInterfaceValid(277));
    }
}
