package scripts.modules.runemysteries.missions.exittower.decisions;

import org.tribot.api2007.Player;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.runemysteries.missions.exittower.processes.WalkToLadder;

public class ShouldWalkLadder extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Player.getPosition().getX() < 3108 && Player.getPosition().getY() < 9575 &&
                Player.getPosition().getY() > 9550;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new WalkToLadder());
        setFalseNode(new ShouldClimbLadder());
    }

}
