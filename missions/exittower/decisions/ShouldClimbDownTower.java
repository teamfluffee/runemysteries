package scripts.modules.runemysteries.missions.exittower.decisions;

import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSTile;
import scripts.fluffeesapi.game.genericProcessNodes.NavigateChangePlaneObject;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.runemysteries.missions.exittower.processes.ExitTower;

public class ShouldClimbDownTower extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        RSTile playerPosition = Player.getPosition();
        return playerPosition.getPlane() > 0 && playerPosition.getX() > 3100 && playerPosition.getY() < 3165;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new NavigateChangePlaneObject("Staircase", "Climb-down"));
        setFalseNode(new ExitTower());
    }

}
