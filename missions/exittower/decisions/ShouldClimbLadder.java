package scripts.modules.runemysteries.missions.exittower.decisions;

import org.tribot.api2007.Player;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.runemysteries.missions.exittower.processes.ClimbLadder;

public class ShouldClimbLadder extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return Player.getPosition().getY() > 9550; //We're still underground
    }

    @Override
    public void initializeNode() {
        setTrueNode(new ClimbLadder());
        setFalseNode(new ShouldClimbDownTower());
    }

}
