package scripts.modules.runemysteries.missions.exittower;

import org.tribot.api2007.Game;
import org.tribot.api2007.Player;
import scripts.fluffeesapi.data.structures.bag.BagIds;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.TreeMission;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;
import scripts.modules.runemysteries.RuneMysteries;
import scripts.modules.runemysteries.missions.exittower.decisions.ShouldWalkLadder;

public class ExitTowerMission implements TreeMission {

    @Override
    public BaseDecisionNode getTreeRoot() {
        return new ShouldWalkLadder();
    }

    @Override
    public String getMissionName() {
        return "Exit tower.";
    }

    @Override
    public String getBagId() {
        return BagIds.RUNE_MYSTERIES.getId();
    }

    @Override
    public boolean isMissionValid() {
        return (Game.getSetting(RuneMysteries.QUEST_SETTING) == 3 || Game.getSetting(RuneMysteries.QUEST_SETTING) == 6)
                && (Player.getPosition().getY() > 9550 || Player.getPosition().getY() < 3167);
    }

    @Override
    public boolean isMissionCompleted() {
        return (Game.getSetting(RuneMysteries.QUEST_SETTING) == 3 || Game.getSetting(RuneMysteries.QUEST_SETTING) == 6)
                && Player.getPosition().getY() < 9550 && Player.getPosition().getY() > 3167;
    }
}
