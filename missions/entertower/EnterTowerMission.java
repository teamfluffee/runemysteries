package scripts.modules.runemysteries.missions.entertower;

import org.tribot.api2007.Game;
import org.tribot.api2007.Player;
import scripts.fluffeesapi.data.structures.bag.BagIds;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.TreeMission;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;
import scripts.modules.runemysteries.RuneMysteries;
import scripts.modules.runemysteries.missions.entertower.decisions.ShouldWalkToTower;

public class EnterTowerMission implements TreeMission {

    @Override
    public BaseDecisionNode getTreeRoot() {
        return new ShouldWalkToTower();
    }

    @Override
    public String getMissionName() {
        return "Entering Tower";
    }

    @Override
    public String getBagId() {
        return BagIds.RUNE_MYSTERIES.getId();
    }

    @Override
    public boolean isMissionValid() {
        return (Game.getSetting(RuneMysteries.QUEST_SETTING) == 1 || Game.getSetting(RuneMysteries.QUEST_SETTING) == 2 ||
                Game.getSetting(RuneMysteries.QUEST_SETTING) == 5) && !RuneMysteries.SEDRIDOR_AREA.contains(Player.getPosition());
    }

    @Override
    public boolean isMissionCompleted() {
        return RuneMysteries.SEDRIDOR_AREA.contains(Player.getPosition());
    }
}
