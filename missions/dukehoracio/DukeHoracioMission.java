package scripts.modules.runemysteries.missions.dukehoracio;

import org.tribot.api2007.Game;
import scripts.fluffeesapi.data.structures.bag.BagIds;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.TreeMission;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;
import scripts.modules.runemysteries.RuneMysteries;
import scripts.modules.runemysteries.missions.dukehoracio.decisions.ShouldTalkDuke;

public class DukeHoracioMission implements TreeMission {
    @Override
    public BaseDecisionNode getTreeRoot() {
        return new ShouldTalkDuke();
    }

    @Override
    public String getMissionName() {
        return "Duke Horacio";
    }

    @Override
    public String getBagId() {
        return BagIds.RUNE_MYSTERIES.getId();
    }

    @Override
    public boolean isMissionValid() {
        return Game.getSetting(RuneMysteries.QUEST_SETTING) == 0;
    }

    @Override
    public boolean isMissionCompleted() {
        return Game.getSetting(RuneMysteries.QUEST_SETTING) >= 1;
    }
}
