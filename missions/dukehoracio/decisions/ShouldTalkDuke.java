package scripts.modules.runemysteries.missions.dukehoracio.decisions;

import org.tribot.api2007.NPCs;
import org.tribot.api2007.types.RSTile;
import scripts.fluffeesapi.game.genericProcessNodes.TalkToGuide;
import scripts.fluffeesapi.game.genericProcessNodes.WalkToLocation;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;

public class ShouldTalkDuke extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return NPCs.findNearest("Duke Horacio").length > 0;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new TalkToGuide("Duke Horacio", "Talking to Duke",
                new String[]{"Have you any quests for me?", "Sure, no problem."}));
        setFalseNode(new WalkToLocation(new RSTile(3210, 3222, 1), "Walking to Duke"));
    }

}
