package scripts.modules.runemysteries.missions.aubury.decisions;

import org.tribot.api2007.NPCs;
import org.tribot.api2007.types.RSTile;
import scripts.fluffeesapi.game.genericProcessNodes.TalkToGuide;
import scripts.fluffeesapi.game.genericProcessNodes.WalkToLocation;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;

public class ShouldWalkAubury extends ConstructorDecisionNode {

    @Override
    public boolean isValid() {
        return NPCs.findNearest("Aubury").length < 1;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new WalkToLocation(new RSTile(3253,3401,0), "Walk to Aubury"));
        setFalseNode(new TalkToGuide("Aubury", "Talk to Aubury",
                new String[]{"I have been sent here with a package for you."}));
    }

}
