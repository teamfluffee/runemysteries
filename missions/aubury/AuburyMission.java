package scripts.modules.runemysteries.missions.aubury;

import org.tribot.api2007.Game;
import org.tribot.api2007.Player;
import scripts.fluffeesapi.data.structures.bag.BagIds;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.TreeMission;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;
import scripts.modules.runemysteries.RuneMysteries;
import scripts.modules.runemysteries.missions.aubury.decisions.ShouldWalkAubury;

public class AuburyMission implements TreeMission {

    @Override
    public BaseDecisionNode getTreeRoot() {
        return new ShouldWalkAubury();
    }

    @Override
    public String getMissionName() {
        return "Aubury mission";
    }

    @Override
    public String getBagId() {
        return BagIds.RUNE_MYSTERIES.getId();
    }

    @Override
    public boolean isMissionValid() {
        return (Game.getSetting(RuneMysteries.QUEST_SETTING) == 3 || Game.getSetting(RuneMysteries.QUEST_SETTING) == 4)
                && Player.getPosition().getY() < 9550 && Player.getPosition().getY() > 3167;
    }

    @Override
    public boolean isMissionCompleted() {
        return Game.getSetting(RuneMysteries.QUEST_SETTING) == 5;
    }
}
