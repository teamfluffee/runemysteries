package scripts.modules.runemysteries;

import org.tribot.api2007.Game;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;
import scripts.fluffeesapi.data.structures.bag.BagIds;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.Mission;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.MissionManager;
import scripts.modules.runemysteries.missions.aubury.AuburyMission;
import scripts.modules.runemysteries.missions.dukehoracio.DukeHoracioMission;
import scripts.modules.runemysteries.missions.entertower.EnterTowerMission;
import scripts.modules.runemysteries.missions.exittower.ExitTowerMission;
import scripts.modules.runemysteries.missions.sedridor.SedridorMission;

import java.util.LinkedList;

public class RuneMysteries implements MissionManager {

    public static final int QUEST_SETTING = 63;
    public static final RSArea SEDRIDOR_AREA = new RSArea(new RSTile[] {
            new RSTile(3095, 9575, 0), new RSTile(3108, 9575, 0),
            new RSTile(3108, 9565, 0), new RSTile(3095, 9565, 0)
    });

    private LinkedList<Mission> missionList = new LinkedList<>();

    @Override
    public LinkedList<Mission> getMissions() {
        if (missionList.isEmpty()) {
            missionList.add(new DukeHoracioMission());
            missionList.add(new EnterTowerMission());
            missionList.add(new SedridorMission());
            missionList.add(new ExitTowerMission());
            missionList.add(new AuburyMission());
        }
        return missionList;
    }

    @Override
    public boolean shouldLoopMissions() {
        return true;
    }

    @Override
    public String getMissionName() {
        return "Rune Mysteries";
    }

    @Override
    public String getBagId() {
        return BagIds.RUNE_MYSTERIES.getId();
    }

    @Override
    public boolean isMissionValid() {
        return Game.getSetting(RuneMysteries.QUEST_SETTING) <= 6;
    }

    @Override
    public boolean isMissionCompleted() {
        return Game.getSetting(RuneMysteries.QUEST_SETTING) == 6 && Player.getPosition().getY() < 9550 &&
                Player.getPosition().getY() > 3164;
    }
}
